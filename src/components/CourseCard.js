import {Card, Button} from 'react-bootstrap';
import {useState} from 'react';


function CourseCard(props) {
  //console.log(props);
  //console.log(typeof props)
  const  {name, description, price} = props.courseProp;

  // react hooks - useState -> store its state
  //Syntax:
    //const [getter, setter] = useState(initialGetterValue)
  const [count, setCount] = useState(0);
  const [countSeat, SeatsCount] = useState(10);
  

  function enroll(){
    
    if(countSeat === 0){
      alert("No more seats available, check back later")
    }else{
      setCount(count + 1);
      SeatsCount(countSeat - 1)
    }
  }
  
  return (
    <Card style={{ width: '100%' }} className="p-3 mb-3">
      
      <Card.Body>
        <Card.Title className="mb-3">{name}</Card.Title>
        <Card.Text>
          <strong>Course description:</strong>
          <br/>
         {description}
        </Card.Text>
        <Card.Text>
          <strong>Course price:</strong>
          <br/>
         {price}
        </Card.Text>
        <Card.Text>Enrollees: {count}</Card.Text>
        <Card.Text>Seats: {countSeat}</Card.Text>
        <Button variant="primary" onClick={enroll}>Enroll</Button>
      </Card.Body>
    </Card>
  );
}

export default CourseCard;